use crossterm::{
    cursor::{DisableBlinking, MoveTo},
    execute,
};
use glam::{Vec2, Vec3};
use itertools::Itertools;
use std::io::stdout;
use std::time::Instant;

mod raytracing;
use raytracing::sphere::*;
use raytracing::*;

// Convert a 0..1 (ish) value into increasingly visually imposing characters
fn render_val(x: &f32) -> char {
    match *x {
        a if a < 0.05 => ' ',
        a if a < 0.25 => '.',
        a if a < 0.45 => '-',
        a if a < 0.65 => '=',
        a if a < 0.85 => '%',
        _ => '@',
    }
}

fn make_camera_ray(pixel: Vec2, origin: Vec3, target: Vec3) -> Ray {
    let world_up = Vec3::new(0., 1., 0.);
    let forward = (target - origin).normalize();
    let right = forward.cross(world_up).normalize();
    let up = right.cross(forward).normalize();
    Ray {
        origin: origin,
        dir: (forward + up * pixel.y + right * pixel.x).normalize(),
    }
}

const SCENE_SPH_RAD: f32 = 1.5;
const BIG: f32 = 100000.;

fn make_scene(time: &f32) -> Vec<Sphere> {
    (-4..5)
        .map(|n| Sphere {
            centre: Vec3::new(
                n as f32 * SCENE_SPH_RAD * 2.0,
                SCENE_SPH_RAD,
                SCENE_SPH_RAD * (time + 2.0 * n as f32).sin(),
            ),
            radius: SCENE_SPH_RAD,
            albedo: 1.0,
        })
        // add a big sphere below the scene to simulate a floor
        .chain(std::iter::once(Sphere {
            centre: Vec3::new(0., -BIG, 0.),
            radius: BIG,
            albedo: 0.2,
        }))
        .collect::<Vec<Sphere>>()
}

fn terminal_coordinate_space(col: &usize, width: &usize, row: &usize, height: &usize) -> Vec2 {
    let aspect = (*width as f32) / ((*height as f32) * 2.); // multiply height to account for characters usually being taller than they are wide
    (Vec2::new(
        (*col as f32) / (*width as f32),
        (*row as f32) / (*height as f32),
    ) * 2.0
        - Vec2::ONE)
        * Vec2::new(aspect, -1.0)
}

fn cam_origin(time: &f32) -> Vec3 {
    let time_adjust = 0.5 * time;
    Vec3::new(
        time_adjust.sin() * 5.0,
        SCENE_SPH_RAD * 2.0 * 1.05,
        (time_adjust * 0.7).cos() * 5.0 - 3.,
    )
}

fn cam_target(time: &f32) -> Vec3 {
    Vec3::new((time * 0.1).sin() * 3.0, SCENE_SPH_RAD * 1., 0.0)
}

fn render_frame(time: f32, frame_dimensions: (usize, usize)) -> Vec<f32> {
    let my_spheres: Vec<Sphere> = make_scene(&time);
    let my_light = Vec3::new(1.0, 1.0, -1.0).normalize();

    (0..frame_dimensions.1)
        .cartesian_product(0..frame_dimensions.0)
        .map(|(row, col)| {
            let pixel_coord =
                terminal_coordinate_space(&col, &frame_dimensions.0, &row, &frame_dimensions.1);

            let ray = make_camera_ray(pixel_coord, cam_origin(&time), cam_target(&time));

            let hit: Option<Hit> = my_spheres.intersect(&ray).map(|h| {
                // On hit...
                let shadow_ray = Ray {
                    // ... construct a ray from the hit towards the light...
                    origin: h.pos,
                    dir: my_light,
                };
                match &my_spheres.intersect(&shadow_ray) {
                    Some(_) => Hit { albedo: 0.0, ..h }, // ... if that hits, we're in shadow.
                    None => h,
                }
            });
            // Dithering is a (.9. .95. 1.0) multiplyer to brightness based on a checkerboard + half-res checkerboard pattern
            // The effect is that pixels near a boundary may be pushed across it before their neighbours. Dithering.
            let dither: f32 = 1.0 - ((row + col) % 2 + (row / 2 + col / 2) % 2) as f32 * 0.05;
            let val: f32 = match hit {
                Some(h) => {
                    let diffuse = h.norm.dot(my_light).max(0.) * h.albedo;
                    diffuse * dither
                }
                None => 0.0,
            };
            val
        })
        .collect()
}

fn main() {
    let start_time: Instant = Instant::now();
    execute!(stdout(), DisableBlinking).expect("Terminal manipulation failed"); // Doesn't work in many terminals anyway
    loop {
        let time = Instant::now().duration_since(start_time).as_secs_f32();

        execute!(stdout(), MoveTo(0, 0)).expect("Terminal manipulation failed");
        let frame = render_frame(time, term_size::dimensions_stdout().unwrap_or((80, 40)))
            .iter()
            .map(render_val)
            .collect::<String>();
        print!("{}", frame);
    }
}
