use super::{Hit, Intersectable, Ray};
use glam::Vec3;

pub struct Sphere {
    pub centre: Vec3,
    pub radius: f32,
    pub albedo: f32,
}

impl Intersectable for Sphere {
    fn intersect(&self, ray: &Ray) -> Option<Hit> {
        let offset = ray.origin - self.centre;

        let a = ray.dir.dot(ray.dir);
        let b = 2.0 * ray.dir.dot(offset);
        let c = offset.dot(offset) - (self.radius * self.radius);

        let disc = (b * b) - (4.0 * a * c);
        if disc < 0.0 {
            None
        } else {
            let sqrt_disc = disc.sqrt();
            let t0 = (-b - sqrt_disc) / (2.0 * a);
            let t1 = (-b + sqrt_disc) / (2.0 * a);

            if (t1 < 0.) || (t0 < 0.) {
                // we're in front of or inside the sphere
                None
            } else {
                let position = ray.origin + ray.dir * t0;
                let normal = (position - self.centre).normalize();
                Some(Hit {
                    distance: t0,
                    pos: position,
                    norm: normal,
                    albedo: self.albedo,
                })
            }
        }
    }
}
