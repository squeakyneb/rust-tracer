use glam::Vec3;

pub mod sphere;

pub struct Ray {
    pub origin: Vec3,
    pub dir: Vec3,
}

pub struct Hit {
    pub pos: Vec3,
    pub distance: f32,
    pub norm: Vec3,
    pub albedo: f32,
}

pub trait Intersectable {
    fn intersect(&self, ray: &Ray) -> Option<Hit>;
}

pub fn nearest_hit(a: Hit, b: Hit) -> Hit {
    if a.distance < b.distance {
        a
    } else {
        b
    }
}

pub fn nearest_hit_opt(one: Option<Hit>, other: Option<Hit>) -> Option<Hit> {
    match one {
        None => other,
        Some(a) => match other {
            None => Some(a),
            Some(b) => Some(nearest_hit(a, b)),
        },
    }
}

pub trait IterIntersectables {
    fn intersect(self, ray: &Ray) -> Option<Hit>;
}

impl<'a, T, U: 'a> IterIntersectables for T
where
    T: IntoIterator<Item = &'a U>,
    U: Intersectable,
{
    fn intersect(self, ray: &Ray) -> Option<Hit> {
        self.into_iter()
            .map(|s| s.intersect(&ray))
            .reduce(nearest_hit_opt)
            .flatten()
    }
}
